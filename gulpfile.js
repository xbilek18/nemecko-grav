/*
Simplified version of gulpfile suitable for php development.
You can use following commands:
    gulp: 
        OR 
    gulp watch : watch for changes in user/ directory and reload browser on change
*/

'use strict'

var gulp = require('gulp'),
    browserSync = require('browser-sync').create();

var plugins = require('gulp-load-plugins')({
    DEBUG: false,
    pattern: ['*'],
    scope: ['dependencies', 'devDependencies'],
    replaceString: /^gulp(-|\.)/,
    camelize: true,
    lazy: true
});

gulp.task('default', ['watch'], function () { });

gulp.task('watch', function () {
    gulp.watch('user/**/*').on('change', function () {
        browserSync.reload();
    });

    plugins.connectPhp.server({ router: 'system/router.php' }, function () {
        browserSync.init({
            proxy: {
                target: 'localhost:8000',
                reqHeaders: function () {
                    return {
                        host: 'localhost:3000'
                    };
                }
            }
        });
    });
});
