<?php
namespace Grav\Plugin;

use Grav\Common\Plugin;

class HideEditModePlugin extends Plugin
{
    
    public static function getSubscribedEvents()
    {
        return [
            'onAdminTwigTemplatePaths' => ['onAdminTwigTemplatePaths', 0]
        ];
    }
    
    public function onAdminTwigTemplatePaths($event)
    {
        $event['paths'] = [__DIR__ . '/admin/themes/grav/templates'];
        
        $customPermission =  $this->grav['admin']->addPermissions(['admin.enable-expert-mode' => 'boolean']);
    }

}
