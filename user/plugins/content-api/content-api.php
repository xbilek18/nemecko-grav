<?php
namespace Grav\Plugin;

use Grav\Common\Plugin;
use Grav\Common\Page\Collection;

class contentApiPlugin extends Plugin
{
    public static function getSubscribedEvents() {
        return [
            'onPluginsInitialized' => ['onPluginsInitialized', 0],
        ];
    }

    public function onPluginsInitialized()
    {
        // hijack output so we can deliver as a different format
        // if this value isset
        if (isset($_GET['return-as']) && in_array($_GET['return-as'], array('json', 'xml', 'yaml'))) {
            $this->enable([
                    'onPageInitialized' => ['deliverFormatAs', 0]
                ]);
        }
        // example: /blog?getBlogItemsByYear=2017
        if (isset($_GET['getBlogItemsByYear']) && is_numeric($_GET['getBlogItemsByYear'])){
            $this->enable([
                'onPageInitialized' => ['getBlogItemsByYear', 0]
            ]);
        }
    }

    public function getBlogItemsByYear()
    {
        $year = $_GET['getBlogItemsByYear'];
        $page = $this->grav['page'];
        $childArray = [];  
        
        foreach ($page->children() as $item) {                        
          if(date("Y", $item->date())==$year){
            $itemArray = [];
            $itemArray["title"] = $item->title();
            $itemArray["date"] = date("d/m/Y",$item->date());
            $itemArray["summary"] = $item->summary();
            $itemArray["url"] = $item->url();  
            $headerArray = (array)$item->header();   
            if(array_key_exists("thumbnail", $headerArray)){
                $itemArray["thumbnail"] = $item->media()->get($headerArray["thumbnail"])->cropResize(400,320)->url();
            }                                 
            if(array_key_exists("authors", $headerArray)){
                $itemArray["authors"] = $headerArray["authors"];          
            }
            array_push($childArray,$itemArray);           
          }
        }        
        header("Content-Type: application/json");
        echo json_encode($childArray);
        exit();
    }

    public function deliverFormatAs()
    {
        /**
         * @var \Grav\Common\Page\Page $page
         */
        $format = $_GET['return-as'];
        $page = $this->grav['page'];
        $collection = $page->collection('content', false);
        $pageArray = $page->toArray();
        $children = array();
        foreach ($collection as $item) {
          $children[] = $item->toArray();
        }
        $pageArray['children'] = $children;
        if($format == 'json'){
          header("Content-Type: application/json");
          echo json_encode($pageArray);        

        }
        exit();
    }
}