---
title: 'Letní jazykový kurz v Germersheimu s podporou DAAD'
balanceText: false
authors: 'Katka Němcová'
media_order: 0de2469ed4e94d1cd5a656c73d3227e53b3604e3-74big.jpeg
articleImage: 0de2469ed4e94d1cd5a656c73d3227e53b3604e3-74big.jpeg
date: '04-02-2018 18:29'
---

##### Mnoho německých univerzit pořádá během letních prázdnin mezinárodní letní kurzy, na které se může přihlásit kdokoliv!
===

**Většinou se jedná o jazykové kurzy, ve kterých si účastníci prohlubují své znalosti německého jazyka, který pilují na hodinách různými cvičeními, ale je zde i nabídka kurzů technického zaměření a z oblasti lékařství. Některé kurzy se blíže zabývají německými reáliemi, nebo německou literaturou. Nabídka kurzů je vskutku široká! A pokud si myslíte, že to není nic pro vás z  důvodu, že neumíte dobře německy, nezoufejte! Některé kurzy nevyžadují excelentní znalosti němčiny, ale jen základy! Letní školy v Německu ale nejsou jen o učení, nabízí většinou i pestrý rámcový program s poznávacími výlety a jinými doprovodnými akcemi. **

To se samozřejmě odráží i na jejich ceně. Aby však kurzy byly dostupné pro všechny, co se zajímají o německý jazyk a kulturu, nabízí DAAD stipendium, které buď z části, nebo plně pokryje cenu kurzu. O stipendium se mohou ucházet jen studenti druhých a vyšších ročníků bakalářského, nebo magisterského studia a je nutné si o něj zažádat nejpozději do 1. prosince každého roku. Kompletní nabídku kurzů, pro které se stipendium nabízí, najdete spolu s bližšími informacemi na [stránkách DAAD](www.daad.cz).


Já sama jsem letos tohoto stipendia využila a jela jsem na mezinárodní letní školu do Germersheimu, která se specializovala na překládání a tlumočení. Pestrá výuka zde probíhala každý den jak v němčině, tak i v češtině. Nezabývali jsme se jen překlady, ale vyzkoušeli jsme si i různé druhy tlumočení v praxi, přičemž nejvíce jsme se věnovali simultánnímu tlumočení. K tomu jsme měli i hodiny zaměřené právnickou a ekonomickou terminologii.

V rámci tohoto kurzu jsme měli i velmi pestrý doprovodný program. Takřka každý den se něco dělo! Měli jsme hudební, filmové a taneční večery, grilování, posezení se všemi účastníky kurzu, prohlídku města, nebo pěší výlet do okolní krajiny. Jezdili jsme na mnoho výletů, navštívili jsme Heidelberg, Speyer, Schwarzwald, lázeňské městečko Baden-Baden, Karlsruhe, nebo i francouzský Štrasburk.

Z mé vlastní zkušenosti mohu říct, že to opravdu stojí za to. Je to skvělý zážitek, během kterého si nejen procvičíte němčinu, ale především poznáte Německo a německou kulturu na vlastní pěst a to se Vám v životě neztratí!  
