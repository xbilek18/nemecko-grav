---
title: 'Prezentace Do Německa na zkušenou na tvé univerzitě'
balanceText: false
authors: 'Alena Fialová'
media_order: mapadnnz.jpg
articleImage: mapadnnz.jpg
published: true
date: '21-09-2017 20:03'
publish_date: '21-09-2017 20:02'
---

Přemýšlíš o vycestování za prací nebo studiem? Láká tě vycestovat co nejlevněji nebo úplně zadarmo? Chtěl/a bys ses dozvědět ještě o jiných nabídkách než je Erasmus+? 

===


Zúčastnil/a ses výměny mládeže nebo workcampu v Německu, teď jsi nováček na vysoké a už chceš zas balit kufr… Nebo ti na střední chyběla odvaha, pojmy jako workcamp, Evropská dobrovolná služba nebo Erasmus ti moc neříkají, ale na výšce se konečně odhodláváš někam vyjet? Ať tak nebo tak, vyraz na své univerzitě na prezentaci Do Německa na zkušenou, kde se dozvíš vše, co potřebuješ!

![](mapadnnz.jpg)


Proč jet na zkušenou zrovna do Německa? Zažiješ mezinárodní prostředí, Německo je náš největší obchodní partner, můžeš si vyšlapat cestičku k budoucímu zaměstnání a v neposlední řadě vylepšit jazyk. Osamostatníš se, a při tom nebudeš na konci světa, ale skoro „za humny“.

### Obsah informačního setkání

* Informace, kde hledat vhodné nabídky a kam se obrátit
* Workcampy, Evropská dobrovolná služba
* Podrobnosti o programu Erasmus+, DAAD stipendiích aj.
* Praktické rady od studentů, kteří již absolvovali podobné pobyty 

Jaké akce pro vás v nejbližší době chystáme? Začneme s prezentacemi na Moravě a postupně se budeme přibližovat až skoro k hranicím s Německem. V pondělí 9. října přijedeme s prezentací na [Ostravskou univerzitu](https://www.facebook.com/events/721616998021910/?acontext=%7B%22action_history%22%3A%22[%7B%5C%22surface%5C%22%3A%5C%22page%5C%22%2C%5C%22mechanism%5C%22%3A%5C%22page_upcoming_events_card%5C%22%2C%5C%22extra_data%5C%22%3A[]%7D]%22%2C%22has_source%22%3Atrue%7D), a hned o den později, 10. 10. se podíváme hned do dvou měst, [Zlína](https://www.facebook.com/events/119061255438764/?acontext=%7B%22action_history%22%3A%22[%7B%5C%22surface%5C%22%3A%5C%22page%5C%22%2C%5C%22mechanism%5C%22%3A%5C%22page_upcoming_events_card%5C%22%2C%5C%22extra_data%5C%22%3A[]%7D]%22%2C%22has_source%22%3Atrue%7D) a [Pardubic](https://www.facebook.com/events/360524424385242/?acontext=%7B%22source%22%3A5%2C%22page_id_source%22%3A109715765713011%2C%22action_history%22%3A[%7B%22surface%22%3A%22page%22%2C%22mechanism%22%3A%22main_list%22%2C%22extra_data%22%3A%22%7B%5C%22page_id%5C%22%3A109715765713011%2C%5C%22tour_id%5C%22%3Anull%7D%22%7D]%2C%22has_source%22%3Atrue%7D). Turné bude pokračovat 11. a 12. října, ve středu 11. 10. na [Pedagogické fakultě Univerzity Karlovy ](https://www.facebook.com/events/349642072125704/?acontext=%7B%22action_history%22%3A%22[%7B%5C%22surface%5C%22%3A%5C%22page%5C%22%2C%5C%22mechanism%5C%22%3A%5C%22page_upcoming_events_card%5C%22%2C%5C%22extra_data%5C%22%3A[]%7D]%22%2C%22has_source%22%3Atrue%7D)a ve čtvrtek 12. 10. zůstaneme v Praze na [Vysoké škole ekonomické](https://www.facebook.com/events/485382775176546/?acontext=%7B%22source%22%3A5%2C%22page_id_source%22%3A109715765713011%2C%22action_history%22%3A[%7B%22surface%22%3A%22page%22%2C%22mechanism%22%3A%22main_list%22%2C%22extra_data%22%3A%22%7B%5C%22page_id%5C%22%3A109715765713011%2C%5C%22tour_id%5C%22%3Anull%7D%22%7D]%2C%22has_source%22%3Atrue%7D). Následující týden  pokračujeme dál v úterý 17. října, a to do [Plzně](https://www.facebook.com/events/379236982496905/?acontext=%7B%22source%22%3A5%2C%22page_id_source%22%3A109715765713011%2C%22action_history%22%3A[%7B%22surface%22%3A%22page%22%2C%22mechanism%22%3A%22main_list%22%2C%22extra_data%22%3A%22%7B%5C%22page_id%5C%22%3A109715765713011%2C%5C%22tour_id%5C%22%3Anull%7D%22%7D]%2C%22has_source%22%3Atrue%7D).

Že jste mezi chystanými prezentacemi nenašli svou vysokou školu? Nezoufejte, v druhé polovině října se chystáme také na Mendelovu i Masarykovu univerzitu v Brně, do Hradce Králové, Ústí nad Labem, Liberce či Olomouce. 

Sledujte náš [Facebook](https://www.facebook.com/schulprojekt/) či web a termíny a bližší informace vám neuniknou!

