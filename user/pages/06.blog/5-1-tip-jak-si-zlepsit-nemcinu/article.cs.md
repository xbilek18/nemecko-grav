---
title: '5 + 1 tip, jak si zlepšit němčinu pokud právě nejste v Německu'
balanceText: true
authors: 'Daniela Orgoníková'
media_order: '2.jpg,1.jpg,2.jpg,3.png,4.png,5.png,6.png,Intro_Deutsch.jpeg'
articleImage: Intro_Deutsch.jpeg
published: true
date: '13-09-2017 15:37'
publish_date: '01-09-2017 13:20'
thumbnail: Intro_Deutsch.jpg
---

Vrátili jste se zpátky z Německa a neradi byste všechno zapomněli? Přinášíme vám pět plus jeden tip, jak si i doma procvičit němčinu a občas si zašprechtit.



===





### 1. Konverzujte německy i v Česku

Spolek IC CUNI při Univerzitě Karlově spravuje databázi studentů, kteří se chtějí seznámit s novými lidmi a navíc se navzájem učit libovolný jazyk. Zkuste to jako naše koordinátorka Terka, která si našla svého konverzačního parťáka z Německa na facebookové skupině [IC CUNI Tandem Teaching Programme](https://www.facebook.com/groups/1511841875797201/?fref=ts!).

Databázi najdete na [www.tandem.ic-cuni.cz](http://www.tandem.ic-cuni.cz)

![](2.jpg)

### 2. Dívejte se na německé filmy

Věděli jste, že v Praze a v Brně probíhá každé léto DAS FILM FEST - festival německy mluvených filmů? Pokud tento článek čtete ještě v létě, nezapomeňte se podívat na [program](https://www.goethe.de/ins/cz/de/ver.cfm?fuseaction=events.detail&event_id=20980499).

![](3.png)

### 3. Procvičujte si slovíčka na sociálních sítích
Slovíčko denně se naučíte na [Facebooku](https://www.facebook.com/daysofdeutsch/) nebo Instagramu [Days of Deutsch](https://www.facebook.com/daysofdeutsch/)!

![](6.png)

### 4. Zažeňte nudu němčinou
Jak efektivněji strávit čas v MHD nebo čekárně než procvičováním němčiny? Pokud máte mobil s androidem, na Google Play najdete nespočet aplikací, s kterými si můžete procvičit slovíčka, fráze nebo gramatiku.

![](5.png)

### 5. Navštěvujte kurz
Další možností, jak si zlepšit němčinu jsou jazykové kurzy. My za Do Německa na zkušenou z vlastní zkušenosti doporučujeme kurzy v Goethe Institutu, kde najdete širokou nabídku od úrovně A1 až po C2.
![](4.png) {.medium}

[Nabídka kurzů v Goethe Institutu](https://www.goethe.de/ins/cz/cs/spr/kur/gia.html)
 (Zápis do kurzů probíhá v 12. - 13. září 2017)


Goethe Institut nabízí rychlý úspěch v učení díky vysoce kvalifikovaným učitelům, nejmodernějším metodám výuky, intenzivní podpoře a poradenství i celosvětově platnému systému jazykových kurzů podle jednotlivých stupňů.

### BONUS: Plánujte další cesty do Německa

Nabídky stáží, dobrovolnických projektů a dalších nabídek sledujte na [Facebooku Do Německa na zkušenou.](https://www.facebook.com/schulprojekt/?fref=ts)
![](1.jpg)
