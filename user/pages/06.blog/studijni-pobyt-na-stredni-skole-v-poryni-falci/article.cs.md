---
title: 'Studijní pobyt na střední škole v Porýní-Falci'
balanceText: false
authors: 'Eva Müllerová'
media_order: a9b45bc8ca76597db25e4a761e8ba98b5cb3f17f-montabaurfachwerkhauseramgroenmarktmontabaurhalf-timberedhousesonthemarketsquare-geographorguk-7630.jpeg
articleImage: a9b45bc8ca76597db25e4a761e8ba98b5cb3f17f-montabaurfachwerkhauseramgroenmarktmontabaurhalf-timberedhousesonthemarketsquare-geographorguk-7630.jpeg
date: '04-02-2018 14:26'
---

##### Ahoj! Jmenuji se Eva, momentálně už chodím na vysokou, ale ráda bych se s Vámi podělila o svou zkušenost se studijním pobytem na střední škole.
===

**Všechno to vzniklo díky rodinné návštěvě našich známých v Mayenu. Ti přišli s nápadem, abych strávila nějaký čas u jejich dcery, která učí na gymnáziu v Montabauru. Já, tou dobou patnáctiletá puberťačka, jsem o tom nejdříve nechtěla ani slyšet. Představa zanechání všech zájmů, rodiny a přátel doma a vydání se do neznáma mě rozhodně nijak nepřitahovala.**

Naštěstí mám rozumné rodiče a příbuzné, kteří mě tak dlouho přesvěčovali, že jsem v srpnu roku 2011 opravdu odjela. No, odjela. Nechala jsem se odvézt s většinou svých věcí do vesničky Weroth v Porýní-Falci na západě Německa, kde už mě čekali dva usměvaví rodiče s tříletým Lennardem, nejroztomilejším a nejšikovnějším chlapečkem, kterého znám. A tak začalo mé sedmiměsíční dobrodružství plné nečekaných momentů, trapasů spojených s nedostatečnou znalostí němčiny, ale především nových přátel a zkušeností.


**První den ve škole**

První den mě do školy ještě zavezli rodiče společně s mou novou "Gastmutter" Sarah, která tam vyučuje dějepis a angličtinu. Ta nás zavedla do sborovny a seznámila s paní, jež má zahraniční studenty na starosti. Naštěstí jsem tam nebyla sama. Ve sborovně kromě mě nervózně přešlapovala ještě jedna slečna v mém věku (z Finska), která stejně jako já rozuměla tak každé páté větě. Obě jsme byly provedeny školou, seznámeny se svými třídními učiteli a poté odvedeny do tříd. Co mě překvapilo nejvíc, už první den školy bylo vyučování v plném proudu, právě probíhala hodina fyziky, kterou jsem svým příchodem narušila. Hned po vstupu do třídy na mě zamávaly tři holky, které se mě poté mile ujaly a pomohly se ve třídě a škole rychle zorientovat.


**Jet nebo zůstat doma? **

Jak to tak bývá, ne všechny dny bych si zopakovala, občas to trochu skřípalo - ať ve škole či v rodině, ale kdybych se měla znovu rozhodnout, zda to vše podniknout znovu či ne, neváhala bych ani vteřinu. Kromě docházky do školy, hlídání Lennarda a drobných výletů a návštěv s rodinou jsem jednou týdně navštěvovala tréninky taneční skupiny Esperanza ve své vesničce, se kterou se pojí moje nejlepší vzpomínky na Německo. Vystupovala jsem s nimi na únorovém karnevalu, zúčastnila se velkolepého průvodu a vyhrála pohár na taneční soutěži. Kromě toho jsem si se všemi holkami tak skvěle sedla, že se pravidelně v únoru vracím a několik z nich mě již přijelo navštívit do Prahy. O výrazném zlepšení jazyka snad ani není potřeba mluvit. Po návratu jsem odmaturovala z němčiny a krátce poté si udělala Goethe Zertifikat B2.

Závěrem chci říct, že studijní pobyty sice představují velký krok z komfortní zóny, ale přinesou člověku víc, než by byl ochoten připustit. 

> Máte chuť vyrazit na střední do zahraničí? My za Do Německa na zkušenou doporučujeme organizaci AFS a jejich [tříměsíční program s častečným stipendiem](http://www.afs.cz/programs/tri-mesice-na-stredni-skole-v-nemecku/). 


