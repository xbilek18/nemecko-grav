---
title: 'Do Německa na zkušenou hledá nové referenty'
balanceText: false
articleImage: 2a2f25427deb52816874112b851b612440eb3f58-dnnz1.jpeg
date: '14-01-2018 10:47'
---

##### Přihlas se na stáž v česko-německém projektu Do Německa na zkušenou!

===

<br>

Chceš mít nejlepší přehled o možnostech studia, praxích, či dobrovolnických pobytech v zahraničí?

Zajímá Tě práce s mladými lidmi?

Máš zkušenosti se zahraničním pobytem v Německu nebo bys je chtěl/a získat?

Chtěl/a by ses podílet na zavedeném česko-německém projektu a získat zkušenosti s PR a marketingem, fundraisingem nebo prací s lidmi?

Přihlaš se na stáž v česko-německém projektu Do Německa na zkušenou!



## **CO ZÍSKÁŠ?**

* sebevědomé vystupování na veřejnosti 

* přehled stipendií, česko-německých organizací, výměnných pobytů + tipy a triky, jak uspět

* finanční ohodnocení a závěrečný certifikát 

* spolupráce na realizaci projektu - na reálných úkolech v praxi dle vlastního výběru:
 
**PR a marketing** – komunikace s veřejností, správa sociálních sítí, organizace účasti na akcích, rešerše novinek a nabídek

**Fundraising** - tvorba finančních žádostí, vyjednávání s partnery, hledání nových sponzorů, plánování rozpočtu projektu

**HR**  – metody neformálního vzdělávání, komunikace s učiteli, rozvoj prezentačních dovedností, team-building, tvorba a aktualizace prezentací a handoutů


## **CO TĚ ČEKÁ?**

1. ** účast na školeních: **
- povinné vstupní dvoudenní školení v Praze 23. - 24. února 2018 
- cestovné na školení je plně uhrazeno
- tematické workshopy (jednodenní školení na cca 4 hodiny) 

2. ** minimálně 3 prezentace na středních nebo vysokých školách**
- informace a motivace ke studiu a jiným aktivitám v Německu (workcampy, EVS, jazykové kurzy aj.) 

3. ** práce ve skupině 3-4 lidí **
- spolupráce při zajištění chodu projektu – práce na konkrétních úkolech 
- podporu zajišťují zkušení koordinátoři 


4.  **realizace projektu od dubna 2018 do března 2019**

Zkušenosti v oblasti managementu ani česko-německých vztahů NEJSOU NUTNÉ. Zkušenosti z různorodých pobytů v zahraničí VÍTANÉ! Přivítáme zájemce z celé České republiky. Základem je pozitivní přístup, touha naučit se něco nového a chtít spolupracovat v týmu! 

**Zašli nám svůj strukturovaný životopis a stručný motivační dopis na nazkusenou@cnfm.cz s předmětem „STÁŽ“ do 31.1. 2018**