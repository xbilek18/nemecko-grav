---
title: 'Studium v Bavorsku: <br> Germanistika na univerzitě ve Würzburgu'
balanceText: false
authors: 'Veronika Malantová'
media_order: 7ff4d09e3bab8acf8eb7f261c10b454055d45d7b-15356482101544094297662848513184875428617192n.jpeg
articleImage: 7ff4d09e3bab8acf8eb7f261c10b454055d45d7b-15356482101544094297662848513184875428617192n.jpeg
date: '28-09-2017 19:31'
thumbnail: 15356482_10154409429766284_8513184875428617192_n.jpg
---

Sedím ve vlaku z Německa a najednou to na mne dopadlo. Je konec Erasmu! Kamarády, které jsem za celý rok strávený v Německu poznala, už nebudu vídat každý týden, ale bůhví kdy...


===




Sedím ve vlaku z Německa a najednou to na mne dopadlo. Je konec Erasmu! Kamarády, které jsem za celý rok strávený v Německu poznala, už nebudu vídat každý týden, ale bůhví kdy. Ještě téhož dne si říkám, že se vrátím a že si určitě podám přihlášku na magisterský program na některé z německých univerzit.
Po svém návratu do Brna jsem se tedy začala zajímat o možnostech magisterského studia na německých univerzitách, podmínkách přijetí atd. A tak se pomalu roztočil byrokratický kolotoč, kdy jsme si musela zařídit všechny potřebné doklady – doporučení od vysokoškolského pedagoga, certifikát z němčiny a jiných jazyků, úředně ověřený překlad maturitního vysvědčení, certifikáty ze stáží, přeložit životopis a mnoho dalšího.

### Možnosti stipendia
 
Souběžně jsem se hledala organizace, které nabízejí stipendia. Rozhodla jsem se zkusit štěstí a podala jsem si žádost o podporu u BTHA (Bayerisch-Tschechische Hochschulagentur). O pár měsíců později jsem se ke své radosti dozvěděla, že mi stipendium bylo přislíbeno. Tou dobou jsem již věděla, že jsem byla přijata na všechny obory, na které jsem se hlásila. Nakonec jsem se rozhodla pro studium germanistiky na univerzitě ve Würzburgu. S ubytováním jsem neměla problém, zažádala jsem si o pokoj na kolejích a za dva měsíce jsem dostala dopis, že mi byl pokoj přidělen.

### Jak se studuje ve Würzburgu?
 
Toto studium bych určitě doporučila těm, kteří již absolvovali germanistiku na bakalářském stupni, protože to je jeden z předpokladů k přijetí. Würzburská germanistika má více institutů, které jsou utvářeny zájmy výzkumu jednotlivých vyučujících. Líbí se mi, že v hodinách hodně diskutujeme a píšeme seminární práce. Nadto je zde patrná podpora ze strany vyučujících, kteří jsou nám v otázkách studia vždy k dispozici. Díky seminářům mám jedinečnou možnost prohloubit svoje odborné znalosti ze všech germanistických disciplín. V neposlední řadě oceňuji širokou nabídku univerzitní knihovny. 
 
Pokud tedy váháte, zda to zkusit, jistě jděte do toho. Myslím, že nebudete litovat.


### BTHA (Bayerisch-Tschechische Hochschulagentur)


Česko-bavorská vysokoškolská agentura (Bayerisch-Tschechische Hochschulagentur, BTHA) je projekt koncipovaný do roku 2020, zahájený v roce 2016 na základě rozvojové zprávy pro česko-bavorské příhraničí z prostředků Ministerstva financí, regionálního rozvoje a vlasti Svobodného státu Bavorsko.
Programy finanční podpory BTHA poskytují široké možnosti od stipendií na studijní pobyty, jazykové kurzy, letní a zimní školy v obou zemích přes příspěvky na stáže, zahraniční cesty a exkurze, granty pro bilaterální akademické projekty a konference nebo společnou přípravu projektů až po výzkumná konsorcia univerzit a vysokých škol v Bavorsku a České republice. Více informací hledejte na www.btha.cz
