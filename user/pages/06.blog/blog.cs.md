---
years:
    - '2017'
    - '2018'
child_type: article
title: Blog
content:
    items: '@self.children'
    order:
        by: date
        dir: desc
---

