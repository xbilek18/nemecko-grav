---
projectHeader: 'O Projektu'
projectParagraph: "Projekt Česko-německého fóra mládeže Do Německa na zkušenou – Versuch’s mal in Deutschland probíhá letos již od roku 2005. Hlavní myšlenka – informovat co nejvíce mladých lidí v České republice o jejich možnostech v Německu – vznikla na půdě Česko-německého fóra mládeže z iniciativy Báry Procházkové.\r\n\r\nDo Německa na zkušenou vedou tři koordinátoři, kteří mají na starosti komunikaci se školami a referenty, PR a finance.\r\n\r\nTým projektu tvoří také 20-25 aktivních referentů, kteří již v Německu na zkušené byli. Referenti se současně s koordinátory podílejí na chodu projektu v rámci pracovních skupinek. Více o jednotlivých členech týmu Do Německa na zkušenou se dozvíte [zde](/nas-tym)."
presentationHeader: 'O prezentaci'
presentationParagraph: "### Do Německa na zkušenou\r\n\r\nCílem projektu Do Německa na zkušenou je informovat studenty o možnostech krátkodobých i dlouhodobých pobytů v Německu i v&nbsp;zahraničí obecně. Prezentace obsahuje přehled studijních a dobrovolnických pobytů, stáží, stipendií, brigád, jazykových certifikátů dalších nabídek. Nechceme, aby prezentace byla jen klasickým výkladem, proto klademe velký důraz na oživení prezentace například formou jazykové animace, kvízu nebo videa. Přednášejícími jsou studenti, kteří již zkušenost z pobytu v zahraničí získali. Záměrem projektu je rozšířit studentům obzory a zároveň je motivovat ke studiu němčiny a také k aktivnímu výjezdu do Německa.\r\n\r\n### Co je obsahem prezentace\r\n\r\n* Krátkodobé pobyty - výměny mládeže, programy Erasmus+\r\n* Dlouhodobé pobyty - pobyt na střední škole v Německu, Erasmus, celé studium v Německu\r\n* Dobrovolnictví - workcampy, Evropská dobrovolná služba\r\n* Práce a brigády"
presentationVideo: 'https://youtu.be/kyiiWdXCiXk'
showInquiryButton: true
inquiryButton:
    text: 'Má Vaše škola zájem o prezentaci?'
    url: 'http://www.tandem-org.eu/nazkusenou/admin/animace_new.php'
presentationImage:
    user/pages/02.o-projektu/jumbo.jpg:
        name: jumbo.jpg
        type: image/jpeg
        size: 2013984
        path: user/pages/02.o-projektu/jumbo.jpg
title: 'O projektu'
projectImage:
    'user/pages/02.o-projektu/jpeg (1).jpeg':
        name: 'jpeg (1).jpeg'
        type: image/jpeg
        size: 246129
        path: 'user/pages/02.o-projektu/jpeg (1).jpeg'
---

