---
projectHeader: 'Unser Projekt'
projectParagraph: "Das Projekt des Deutsch-Tschechischen Jugendforums „Versuch’s mal in Deutschland“ existiert seit dem Jahr 2005. Der Hauptgedanke, möglichst viele junge Menschen in der Tschechischen Republik über ihre Möglichkeiten in Deutschland zu informieren, entstand im Deutsch-Tschechischen Jugendforum im Rahmen einer Arbeitsgruppe auf Initiative der Leitung Bára Procházková.\r\n\r\nDas Projekt „Versuch´s mal in Deutschland“ wird von 3 KoordinatorInnen geleitet, die für die Kommunikation mit den Schulen und ReferentInnen, PR und Fundraising zuständig sind.\r\n\r\nUnser Team besteht aus 20 – 25 aktiven ReferentInnen, die über verschiedene Auslandserfahrungen verfügen. Sie kümmern sich um den Verlauf des Projektes in drei Arbeitsgruppen. Mehr über unser Team erfahren Sie [hier](https://dnnz.cz/cs/nas-tym).\r\n\r\n"
presentationHeader: 'Unsere Präsentation'
presentationParagraph: "### Versuch’s mal in Deutschland\r\n\r\n\r\nDas Ziel des Projektes „Versuch´s mal in Deutschland“ ist, die Schüler über ihre Möglichkeiten der kurzen oder langen Aufenthalte in Deutschland und im Ausland allgemein zu informieren. Unsere Präsentation stellt einen Überblick von Studienaufenthalten, Praktiken, Stipendien, Ferienjobs, Freiwilligendienste, Sprachdiplome und andere Angebote dar. Wir möchten nicht nur einen langen Vortrag halten, deshalb legen wir Wert auf Belebung unserer Präsentation z.B. mit einer Sprachanimation, Quiz oder einem kurzem Video. Unsere ausgebildeten ReferentInnen verfügen schon über die Erfahrungen mit verschiedensten Aufenthalten in Deutschland, oder im Ausland allgemein. Der Hauptzweck des Projektes ist, den Schüler ihren Horizont zu erweitern und sie zum Deutschlernen und auch zur Ausreise nach Deutschland zu motivieren.  \r\n\r\n\r\n### Was ist der Inhalt der Präsentation?\r\n\r\n* Kurze Aufenthalte – Jugendaustauschprogramme, Erasmus+\r\n* Lange Aufenthalte – berufliche Lernaufenthalte, Aufenthalte an den deutschen Schulen und Universitäten, das ganze Studium in Deutschland\r\n* Freiwilliger Dienst – Workcamps, Europäischer Freiwilligendienst \r\n* Arbeitsmöglichkeiten und Ferienjobs \r\n"
presentationVideo: 'https://youtu.be/kyiiWdXCiXk'
showInquiryButton: true
inquiryButton:
    text: 'Ist Ihre Schule an der Präsentation interessiert?'
    url: 'http://www.tandem-org.eu/nazkusenou/admin/animace_new.php'
projectImage:
    'user/pages/02.o-projektu/jpeg (1).jpeg':
        name: 'jpeg (1).jpeg'
        type: image/jpeg
        size: 246129
        path: 'user/pages/02.o-projektu/jpeg (1).jpeg'
presentationImage:
    user/pages/02.o-projektu/jumbo.jpg:
        name: jumbo.jpg
        type: image/jpeg
        size: 2013984
        path: user/pages/02.o-projektu/jumbo.jpg
title: 'O projektu'
---

