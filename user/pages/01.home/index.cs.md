---
persons:
    -
        name: 'Jacob Venuß, M.A.'
        position: 'Česko-německý fond budoucnosti'
        description: 'Jährlich kommen rund 4.000 junge tschechische Menschen in den Genuss einer Präsentation. Dies stellt beim Deutsch-Tschechischen Zukunftsfonds im Förderbereich Jugend und Schule einen Spitzenwert dar.'
    -
        name: 'Mgr. Ivana Kopečková'
        position: 'Střední průmyslová škola strojnická a SOŠ prof. Švejcara'
        description: 'Prezentace projektu Do Německa na zkušenou u náš na škole zaujala i takové žáky, které němčina ve škole příliš nebavila a po maturitě se rozhodli vyjet na roční pobyt do Německa.'
    -
        name: 'Veronika Malantová'
        position: 'Studentka univerzity ve Würzburgu'
        description: 'Díky projektu Do Německa na zkušenou jsem získala přehled o workcampech, jazykových pobytech, stážích a stipendiích, za což jsem velmi vděčná. Angažmá v tomto projektu bylo pro mne jedním z impulsů ke studiu germanistiky v německém Würzburgu.'
inquiryButton:
    text: 'Má Vaše škola zájem o prezentaci?'
    url: 'http://www.tandem-org.eu/nazkusenou/admin/animace_new.php'
FAQButton:
    text: 'Přečtěte si nejčastější dotazy'
    url: /faq
jumbo:
    user/pages/01.home/jumbo.jpg:
        name: jumbo.jpg
        type: image/jpeg
        size: 2013984
        path: user/pages/01.home/jumbo.jpg
title: Home
---

