---
name: 'Kateřina Němcová'
date: '28-09-2017 19:12'
job: 'Univerzita Karlova v Praze, obor Německý jazyk a literatura, Němčina pro mezikulturní komunikaci'
involmentReason: ' je to pro mě jako pro člověka, co se dlouhodobě zajímá o německy mluvící země, skvělá příležitost se sama dozvědět co nejvíce informací o nejrůznějších možnostech pobytu v Německu a předávat tyto informace dál'
wordAssociation: 'města Brémy a Hamburg, která bych chtěla v blízké době navštívit a místa, která jsem již v minulosti navštívila'
favoriteWord: 'das Eichhörnchen (veverka)'
title: 'Kateřina Němcová'
image:
    user/pages/03.nas-tym/06.katerina-nemcova/5661887b8bec1f9b6154d2936429c6abce26c999-dsc0168-1.jpeg:
        name: 5661887b8bec1f9b6154d2936429c6abce26c999-dsc0168-1.jpeg
        type: image/jpeg
        size: 72418
        path: user/pages/03.nas-tym/06.katerina-nemcova/5661887b8bec1f9b6154d2936429c6abce26c999-dsc0168-1.jpeg
smallImage:
    user/pages/03.nas-tym/06.katerina-nemcova/cd672c05713cd9a875a76dcc66e3e1801882f0cc-dsc0168a.jpeg:
        name: cd672c05713cd9a875a76dcc66e3e1801882f0cc-dsc0168a.jpeg
        type: image/jpeg
        size: 7402
        path: user/pages/03.nas-tym/06.katerina-nemcova/cd672c05713cd9a875a76dcc66e3e1801882f0cc-dsc0168a.jpeg
---

