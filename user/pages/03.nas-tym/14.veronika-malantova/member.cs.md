---
name: 'Veronika Malantová'
date: '29-09-2017 07:42'
job: 'Germanistik als Fremdsprachenphilologie'
involmentReason: 'od malička miluji cestování a poznávání nových míst a kultur. No a, jelikož jsem sama měla tu možnost strávit rok v Německu a velmi mě to obohatilo, tak bych chtěla tuto svou zkušenost předat dál a motivovat mladé lidi, aby také vyjeli do zahraničí. 🙂  A navíc díky projektu mám možnost setkávat se s mnoha inspirativními lidmi!'
wordAssociation: 'celý rok, který jsem tam strávila a mnoho skvělých lidí, které jsem měla to štěstí poznat 🙂'
favoriteWord: kaputt
title: 'Veronika Malantová'
image:
    user/pages/03.nas-tym/14.veronika-malantova/875b79e7906d69759103356dfda9c0dd7f5dc5de-22014778102039433379803261909337658n.jpeg:
        name: 875b79e7906d69759103356dfda9c0dd7f5dc5de-22014778102039433379803261909337658n.jpeg
        type: image/jpeg
        size: 117262
        path: user/pages/03.nas-tym/14.veronika-malantova/875b79e7906d69759103356dfda9c0dd7f5dc5de-22014778102039433379803261909337658n.jpeg
smallImage:
    user/pages/03.nas-tym/14.veronika-malantova/57d99339b321de6e257ba3395784cf3d39fd0e39-22014778102039433379803261909337658jhfxn.jpeg:
        name: 57d99339b321de6e257ba3395784cf3d39fd0e39-22014778102039433379803261909337658jhfxn.jpeg
        type: image/jpeg
        size: 11164
        path: user/pages/03.nas-tym/14.veronika-malantova/57d99339b321de6e257ba3395784cf3d39fd0e39-22014778102039433379803261909337658jhfxn.jpeg
---

