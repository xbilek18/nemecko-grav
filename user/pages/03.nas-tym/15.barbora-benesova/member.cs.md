---
name: 'Barbora Benešová'
date: '27-12-2017 11:28'
image:
    user/pages/03.nas-tym/15.barbora-benesova/5c4883f65fabb9a9f6f3ef81aae78bf73c20b1a6-dsc0197.jpeg:
        name: 5c4883f65fabb9a9f6f3ef81aae78bf73c20b1a6-dsc0197.jpeg
        type: image/jpeg
        size: 81891
        path: user/pages/03.nas-tym/15.barbora-benesova/5c4883f65fabb9a9f6f3ef81aae78bf73c20b1a6-dsc0197.jpeg
smallImage:
    user/pages/03.nas-tym/15.barbora-benesova/d3dc7a36f0e5d06a08e0d140504663c45d990058-dsc0197-1.jpeg:
        name: d3dc7a36f0e5d06a08e0d140504663c45d990058-dsc0197-1.jpeg
        type: image/jpeg
        size: 7864
        path: user/pages/03.nas-tym/15.barbora-benesova/d3dc7a36f0e5d06a08e0d140504663c45d990058-dsc0197-1.jpeg
job: 'Goethe-Institut, produkce Česko-německé kulturní jaro 2017'
involmentReason: 'se chci podílet na rozvoji česko-německých vztahů, předávat své zkušenosti s pobytem v zahraničí a motivovat tak mladé lidi k výjezdu do Německa'
wordAssociation: 'úspěch, rozmanitost, svoboda'
title: 'Barbora Benešová'
---

