---
name: 'Jitka Holčáková'
date: '28-09-2017 19:11'
job: 'Univerzita Palackého v Olomouci, obor Žurnalistika – Německá filologie'
involmentReason: 'Česko a Německo mají společnou historii, a proto chci studentům ukázat, že by se k němčině neměli otáčet zády'
wordAssociation: 'země, která se poučila ze svých chyb'
favoriteWord: 'lebensfroh (protože mě vystihuje)'
title: 'Jitka Holčáková'
image:
    user/pages/03.nas-tym/05.jitka-holcakova/c1c99e4bb103906b7e77a62f656b7630077247c3-jitkavelka.jpeg:
        name: c1c99e4bb103906b7e77a62f656b7630077247c3-jitkavelka.jpeg
        type: image/jpeg
        size: 85469
        path: user/pages/03.nas-tym/05.jitka-holcakova/c1c99e4bb103906b7e77a62f656b7630077247c3-jitkavelka.jpeg
smallImage:
    user/pages/03.nas-tym/05.jitka-holcakova/6230d27fda34b74acb3f46889dff070b73de86bc-jitkamala.jpeg:
        name: 6230d27fda34b74acb3f46889dff070b73de86bc-jitkamala.jpeg
        type: image/jpeg
        size: 8924
        path: user/pages/03.nas-tym/05.jitka-holcakova/6230d27fda34b74acb3f46889dff070b73de86bc-jitkamala.jpeg
---

