---
name: 'Anežka Doležalová'
date: '28-09-2017 19:10'
job: 'Koordinátorka komiksové soutěže „Promluv k světu bublinou!“'
involmentReason: 'už nejsem aktivním členem Fóra'
wordAssociation: 'písečné pláže u Rostocku, chřest, otevření a přátelští lidé'
favoriteWord: Eichhörnchen
title: 'Anežka Doležalová'
image:
    user/pages/03.nas-tym/03.anezka-dolezalova/20180331120924-62fb657039e92eeeac3e72146a5a9f6fa849d0df-anezkadolezalova.jpeg:
        name: 20180331120924-62fb657039e92eeeac3e72146a5a9f6fa849d0df-anezkadolezalova.jpeg
        type: image/jpeg
        size: 90212
        path: user/pages/03.nas-tym/03.anezka-dolezalova/20180331120924-62fb657039e92eeeac3e72146a5a9f6fa849d0df-anezkadolezalova.jpeg
smallImage:
    user/pages/03.nas-tym/03.anezka-dolezalova/20180331120930-cfb957e0f6283d640b0f8279bab2afef93dd3466-anezkadolezalovasmall.jpeg:
        name: 20180331120930-cfb957e0f6283d640b0f8279bab2afef93dd3466-anezkadolezalovasmall.jpeg
        type: image/jpeg
        size: 8486
        path: user/pages/03.nas-tym/03.anezka-dolezalova/20180331120930-cfb957e0f6283d640b0f8279bab2afef93dd3466-anezkadolezalovasmall.jpeg
---

