---
name: 'Tereza Sudíková'
date: '28-12-2017 12:14'
job: 'Mediální studia FSV UK/Česká televize'
involmentReason: 'zkušenosti ze zahraničí neskutečným způsobem rozšiřují obzory, a proto je důležité informovat o dostupných možnostech, kterých je celá řada.'
wordAssociation: 'vyspělá země'
favoriteWord: 'genau nebo Zusammengehörigkeitsgeführ'
title: 'Tereza Sudíková'
image:
    user/pages/03.nas-tym/17.tereza-sudikova/3bd24396ef81aa3945ebe4b6417b8b3428b265aa-dsc0207.jpeg:
        name: 3bd24396ef81aa3945ebe4b6417b8b3428b265aa-dsc0207.jpeg
        type: image/jpeg
        size: 101202
        path: user/pages/03.nas-tym/17.tereza-sudikova/3bd24396ef81aa3945ebe4b6417b8b3428b265aa-dsc0207.jpeg
smallImage:
    user/pages/03.nas-tym/17.tereza-sudikova/8a516433bb5760f9153c403c237f9a25c534dad9-dsc0207-1.jpeg:
        name: 8a516433bb5760f9153c403c237f9a25c534dad9-dsc0207-1.jpeg
        type: image/jpeg
        size: 9194
        path: user/pages/03.nas-tym/17.tereza-sudikova/8a516433bb5760f9153c403c237f9a25c534dad9-dsc0207-1.jpeg
---

