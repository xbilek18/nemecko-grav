---
name: 'Zuzana Orálková'
date: '28-09-2017 19:19'
job: 'Univerzita Palackého v Olomouci, obor Německá filologie'
involmentReason: 'bych se ráda podělila o svoje zahraniční zkušenosti a ukázala studentům, kolik možností a zážitků jim leží často na dosah ruky.'
wordAssociation: 'památky, Lederhosen, automobilový průmysl'
favoriteWord: Bücherwurm
title: 'Zuzana Orálková'
image:
    user/pages/03.nas-tym/13.zuzana-oralkova/e6f6701b7da264376705894d044def55327ab7a6-zuzanaorlkov.jpeg:
        name: e6f6701b7da264376705894d044def55327ab7a6-zuzanaorlkov.jpeg
        type: image/jpeg
        size: 92900
        path: user/pages/03.nas-tym/13.zuzana-oralkova/e6f6701b7da264376705894d044def55327ab7a6-zuzanaorlkov.jpeg
smallImage:
    user/pages/03.nas-tym/13.zuzana-oralkova/cb9e045313aec3cb7a7c5c64b5365ec284c15de0-zuzanaoralkova-1.jpeg:
        name: cb9e045313aec3cb7a7c5c64b5365ec284c15de0-zuzanaoralkova-1.jpeg
        type: image/jpeg
        size: 9539
        path: user/pages/03.nas-tym/13.zuzana-oralkova/cb9e045313aec3cb7a7c5c64b5365ec284c15de0-zuzanaoralkova-1.jpeg
---

