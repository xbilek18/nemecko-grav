---
name: 'Martina Kukrálová'
date: '28-09-2017 19:16'
job: 'Vysoká škola chemicko-technologická v Praze'
involmentReason: 'chci povzbudit ostatní studenty k výjezdům do zahraničí za studiem, ale i na různorodé dobrovolnické projekty'
wordAssociation: 'náš velký soupeř o zlaté medaile z biatlonu'
favoriteWord: Eintopf
title: 'Martina Kukrálová'
image:
    user/pages/03.nas-tym/09.martina-kukralova/a76c0fefd8b372facccf69e2294a61910ce53a7d-martinakukrlov.jpeg:
        name: a76c0fefd8b372facccf69e2294a61910ce53a7d-martinakukrlov.jpeg
        type: image/jpeg
        size: 66064
        path: user/pages/03.nas-tym/09.martina-kukralova/a76c0fefd8b372facccf69e2294a61910ce53a7d-martinakukrlov.jpeg
smallImage:
    user/pages/03.nas-tym/09.martina-kukralova/6b35e37addec1eae6fbf89b495ed83fd675deaed-martinakukralovaay.jpeg:
        name: 6b35e37addec1eae6fbf89b495ed83fd675deaed-martinakukralovaay.jpeg
        type: image/jpeg
        size: 8880
        path: user/pages/03.nas-tym/09.martina-kukralova/6b35e37addec1eae6fbf89b495ed83fd675deaed-martinakukralovaay.jpeg
---

