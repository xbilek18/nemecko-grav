---
name: 'Michaela Váňová'
date: '28-09-2017 19:18'
job: 'Vysoká škola ekonomická v Praze, fakulta mezinárodních vztahů'
involmentReason: 'ráda jezdím do Německa a mám tam spoustu kamarádů. A přijde mi fajn, motivovat ostatní, aby je měli taky.'
wordAssociation: 'preciznost, kvalita a Berlín'
favoriteWord: einfach
title: 'Michaela Váňová'
image:
    user/pages/03.nas-tym/12.michaela-vanova/c05d40fc2cebb471ba5ec45ca5f8bb0e21a40667-michaelavanova.jpeg:
        name: c05d40fc2cebb471ba5ec45ca5f8bb0e21a40667-michaelavanova.jpeg
        type: image/jpeg
        size: 120137
        path: user/pages/03.nas-tym/12.michaela-vanova/c05d40fc2cebb471ba5ec45ca5f8bb0e21a40667-michaelavanova.jpeg
smallImage:
    user/pages/03.nas-tym/12.michaela-vanova/57b7c854fea64a27c07e8e0cd3f6dc4d341d3d5d-michaelavovsmall.jpeg:
        name: 57b7c854fea64a27c07e8e0cd3f6dc4d341d3d5d-michaelavovsmall.jpeg
        type: image/jpeg
        size: 9308
        path: user/pages/03.nas-tym/12.michaela-vanova/57b7c854fea64a27c07e8e0cd3f6dc4d341d3d5d-michaelavovsmall.jpeg
---

