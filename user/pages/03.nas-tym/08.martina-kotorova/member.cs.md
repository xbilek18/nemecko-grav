---
name: 'Martina Kotorová'
date: '28-09-2017 19:16'
job: 'Německá filologie na Univerzitě Palackého v Olomouci'
involmentReason: 'chci sdílet své zkušenosti a motivovat ostatní k výjezdu do zahraničí'
wordAssociation: 'napadá mě … rozmanitost'
favoriteWord: 'Nilpferd (vždycky mi přišlo dost legrační)'
title: 'Martina Kotorová'
image:
    user/pages/03.nas-tym/08.martina-kotorova/a4b4744e74a3175bea9b0918c6c1797119fcb85b-martinakotorov.jpeg:
        name: a4b4744e74a3175bea9b0918c6c1797119fcb85b-martinakotorov.jpeg
        type: image/jpeg
        size: 105994
        path: user/pages/03.nas-tym/08.martina-kotorova/a4b4744e74a3175bea9b0918c6c1797119fcb85b-martinakotorov.jpeg
smallImage:
    user/pages/03.nas-tym/08.martina-kotorova/d4d04048875f803a4a652a1d6ac663654d2ede85-martinakotorovaaa.jpeg:
        name: d4d04048875f803a4a652a1d6ac663654d2ede85-martinakotorovaaa.jpeg
        type: image/jpeg
        size: 10551
        path: user/pages/03.nas-tym/08.martina-kotorova/d4d04048875f803a4a652a1d6ac663654d2ede85-martinakotorovaaa.jpeg
---

