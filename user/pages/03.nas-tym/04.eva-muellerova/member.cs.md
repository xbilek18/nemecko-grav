---
name: 'Eva Müllerová'
date: '28-09-2017 19:11'
job: 'Mezinárodní obchod na VŠE'
involmentReason: 'chci přispět k tomu, aby co nejvíc mladých lidí mělo přehled o možnostech, jak zažít něco nezapomenutelného a čerpat z toho celý život'
wordAssociation: 'přátelé, čistota a Currywurst mit Pommes'
favoriteWord: Rhabarbermarmelad
title: 'Eva Müllerová'
image:
    user/pages/03.nas-tym/04.eva-muellerova/caec9d5a0c7aa826dad96d998ac0caf0637ecec7-evamullerova.jpeg:
        name: caec9d5a0c7aa826dad96d998ac0caf0637ecec7-evamullerova.jpeg
        type: image/jpeg
        size: 103409
        path: user/pages/03.nas-tym/04.eva-muellerova/caec9d5a0c7aa826dad96d998ac0caf0637ecec7-evamullerova.jpeg
smallImage:
    user/pages/03.nas-tym/04.eva-muellerova/271c8688d69703b832e1fd8cb7c9b5425614e7e9-evamllerovsmall.jpeg:
        name: 271c8688d69703b832e1fd8cb7c9b5425614e7e9-evamllerovsmall.jpeg
        type: image/jpeg
        size: 9129
        path: user/pages/03.nas-tym/04.eva-muellerova/271c8688d69703b832e1fd8cb7c9b5425614e7e9-evamllerovsmall.jpeg
---

