---
name: 'Daniela Orgoníková'
date: '27-12-2017 11:35'
job: 'Mediální studia FSV UK/ HAVAS PR Prague'
involmentReason: 'chci předat co nejvíce lidem mé zkušenosti a informace o možnostech v Německu.'
wordAssociation: 'přesnost, ekologie a občaské vzdělávání (= něco, co nám v ČR chybí).'
favoriteWord: 'Rindfleischetikettierungsüberwachungsaufgabenübertragungsgesetz (= nejdelší německé slovo)'
title: 'Daniela Orgoníková'
image:
    user/pages/03.nas-tym/16.daniela-orgonikova/f4c7efa1777178c859caec6d96be520bebb9adc0-dsc0256.jpeg:
        name: f4c7efa1777178c859caec6d96be520bebb9adc0-dsc0256.jpeg
        type: image/jpeg
        size: 104005
        path: user/pages/03.nas-tym/16.daniela-orgonikova/f4c7efa1777178c859caec6d96be520bebb9adc0-dsc0256.jpeg
smallImage:
    user/pages/03.nas-tym/16.daniela-orgonikova/7fa05bc68462ecedd0cc28f8bddd210138062811-dsc0256-1.jpeg:
        name: 7fa05bc68462ecedd0cc28f8bddd210138062811-dsc0256-1.jpeg
        type: image/jpeg
        size: 9355
        path: user/pages/03.nas-tym/16.daniela-orgonikova/7fa05bc68462ecedd0cc28f8bddd210138062811-dsc0256-1.jpeg
---

