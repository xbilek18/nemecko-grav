---
name: 'Alena Fialová'
questions:
    -
        question: Otázka
        answer: Odpověď
date: '28-09-2017 18:59'
job: 'Západočeská univerzita v Plzni, obor Evropská kulturní studia'
involmentReason: 'česko-německá spolupráce mě baví na jakékoli úrovni a ráda pomůžu studentům z Česka k informacím o tom, jaké možnosti v Německu mají.'
wordAssociation: ' můj milovaný Mnichov, němčina, „perfekt geplannt“, že bych tam už zase jela a taky Mass und Breze!'
favoriteWord: begeistert
title: 'Alena Fialová'
image:
    user/pages/03.nas-tym/01.alena-fialova/c66d6f403c68692233e3181da34bcc7948da990b-alenafialova.jpeg:
        name: c66d6f403c68692233e3181da34bcc7948da990b-alenafialova.jpeg
        type: image/jpeg
        size: 83882
        path: user/pages/03.nas-tym/01.alena-fialova/c66d6f403c68692233e3181da34bcc7948da990b-alenafialova.jpeg
smallImage:
    user/pages/03.nas-tym/01.alena-fialova/891358b679ab701fce690f8da31830d2459b9f25-alenafialovsmall.jpeg:
        name: 891358b679ab701fce690f8da31830d2459b9f25-alenafialovsmall.jpeg
        type: image/jpeg
        size: 9161
        path: user/pages/03.nas-tym/01.alena-fialova/891358b679ab701fce690f8da31830d2459b9f25-alenafialovsmall.jpeg
---

