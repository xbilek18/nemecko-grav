---
name: 'Alexandra Šlesingrová'
date: '28-09-2017 19:09'
image:
    user/pages/03.nas-tym/02.alexandra-slesingrova/f58ade5e8272335e6852d230e547254d96e02ae2-alexandraslesingrova.jpeg:
        name: f58ade5e8272335e6852d230e547254d96e02ae2-alexandraslesingrova.jpeg
        type: image/jpeg
        size: 102228
        path: user/pages/03.nas-tym/02.alexandra-slesingrova/f58ade5e8272335e6852d230e547254d96e02ae2-alexandraslesingrova.jpeg
smallImage:
    user/pages/03.nas-tym/02.alexandra-slesingrova/49851512d96b19fc25da809405078ce46abf76b4-alexandrasmall.jpeg:
        name: 49851512d96b19fc25da809405078ce46abf76b4-alexandrasmall.jpeg
        type: image/jpeg
        size: 8651
        path: user/pages/03.nas-tym/02.alexandra-slesingrova/49851512d96b19fc25da809405078ce46abf76b4-alexandrasmall.jpeg
job: 'Univerzita Hradec Králové, obor Německý jazyk a literatura a Etická výchova'
involmentReason: 'English ist ein Muss, Deutsch ist ein Plus ! Chtěla bych motivovat mladé lidi aby se učili němčinu, němčina otevírá spoustu nových možností do života'
wordAssociation: 'přesnost, disciplína, milí lidé, pivo, currywurst, … a můj roční pobyt na Erasmu v Jeně, na který moc ráda vzpomínám a který mě utvrdil v tom, že němčina má smysl'
favoriteWord: 'Fernweh a Gänseblumchen'
title: 'Alexandra Šlesingrová'
---

