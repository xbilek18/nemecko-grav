---
contactPerson1:
    name: 'Tereza Sudíková'
    position: Finance
    phone: '+420 774 116 390'
    image:
        user/pages/05.kontakt/8a516433bb5760f9153c403c237f9a25c534dad9-dsc0207-1.jpeg:
            name: 8a516433bb5760f9153c403c237f9a25c534dad9-dsc0207-1.jpeg
            type: image/jpeg
            size: 9194
            path: user/pages/05.kontakt/8a516433bb5760f9153c403c237f9a25c534dad9-dsc0207-1.jpeg
contactPerson2:
    name: 'Veronika Malantová'
    position: 'PR, marketing'
    phone: '+420 721 468 416'
    image:
        user/pages/05.kontakt/57d99339b321de6e257ba3395784cf3d39fd0e39-22014778102039433379803261909337658jhfxn.jpeg:
            name: 57d99339b321de6e257ba3395784cf3d39fd0e39-22014778102039433379803261909337658jhfxn.jpeg
            type: image/jpeg
            size: 11164
            path: user/pages/05.kontakt/57d99339b321de6e257ba3395784cf3d39fd0e39-22014778102039433379803261909337658jhfxn.jpeg
contactPerson3:
    name: 'Barbora Benešová'
    position: 'HR, komunikace se školami'
    phone: '+420 776 346 461'
    image:
        user/pages/05.kontakt/d3dc7a36f0e5d06a08e0d140504663c45d990058-dsc0197-1.jpeg:
            name: d3dc7a36f0e5d06a08e0d140504663c45d990058-dsc0197-1.jpeg
            type: image/jpeg
            size: 7864
            path: user/pages/05.kontakt/d3dc7a36f0e5d06a08e0d140504663c45d990058-dsc0197-1.jpeg
phone: '+420 773 987 565'
email: nazkusenou@cnfm.cz
facebook: 'https://www.facebook.com/schulprojekt'
instagram: 'https://www.instagram.com/donemeckanazkusenou/'
title: Kontakt
---

