---
contactPerson1:
    name: 'Tereza Sudíková'
    position: finance
    phone: '+420 774 116 390'
    image:
        user/pages/05.kontakt/Tereza_Sudikova.JPG:
            name: Tereza_Sudikova.JPG
            type: image/jpeg
            size: 736224
            path: user/pages/05.kontakt/Tereza_Sudikova.JPG
contactPerson2:
    name: 'Veronika Malantová'
    position: 'PR, marketing'
    phone: '+420 721 468 416'
    image:
        user/pages/05.kontakt/profil.jpg:
            name: profil.jpg
            type: image/jpeg
            size: 1171872
            path: user/pages/05.kontakt/profil.jpg
contactPerson3:
    name: 'Barbora Benešová'
    position: 'HR, komunikace'
    phone: '+420 776 346 461'
    image:
        user/pages/05.kontakt/Barbora_Benesova.JPG:
            name: Barbora_Benesova.JPG
            type: image/jpeg
            size: 926470
            path: user/pages/05.kontakt/Barbora_Benesova.JPG
phone: '+420 773 987 565'
email: nazkusenou@cnfm.cz
facebook: 'https://www.facebook.com/schulprojekt'
instagram: 'https://www.instagram.com/donemeckanazkusenou/'
title: Kontakt
---

